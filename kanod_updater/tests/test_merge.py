#  Copyright (C) 2022 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from unittest import TestCase
import yaml

from kanod_updater import config_util
from kanod_updater import merge

TEMPLATE_1 = '''
a: 1
b:
  c: 2
  d: 3
g: 6
'''

SPEC_1 = '''
a: 4
b:
  c: 6
  f: 7
e: 5
'''

RESULT_1 = '''
a: 1
b:
  c: 2
  d: 3
  f: 7
e: 5
g: 6
'''

RESULT_2 = '''
a: 4
b:
  c: 2
  d: 3
  f: 7
e: 5
g: 6
'''

RESULT_3 = '''
a: 1
b:
  c: 2
  d: 3
e: 5
g: 6
'''

RESULT_4 = '''
a: 1
b:
  c: 2
  d: 3
e: 5
g: 6
'''

RESULT_5 = '''
a: 1
b:
  c: 2
  d: 3
  f: 7
g: 6
'''

eh = config_util.ErrorHandler()

load = yaml.safe_load


class TestMerge(TestCase):
    def test_merge(self):
        r = merge.merge(eh, {}, load(TEMPLATE_1), load(SPEC_1))
        self.assertEqual(r, load(RESULT_1))

    def test_merge_default(self):
        temp = load(TEMPLATE_1)
        temp[merge.STRATEGY_KEY] = {
            'a': merge.DEFAULT_KEY, 'g': merge.DEFAULT_KEY
        }
        r = merge.merge(eh, {}, temp, load(SPEC_1))
        self.assertEqual(r, load(RESULT_2))

    def test_merge_no_additional(self):
        temp = load(TEMPLATE_1)
        temp[merge.STRATEGY_KEY] = {'b': merge.NO_ADDITIONAL_KEY}
        r = merge.merge(eh, {}, temp, load(SPEC_1))
        self.assertEqual(r, load(RESULT_3))

    def test_fixed(self):
        temp = load(TEMPLATE_1)
        temp[merge.STRATEGY_KEY] = {'b': merge.FIXED_KEY}
        r = merge.merge(eh, {}, temp, load(SPEC_1))
        self.assertEqual(r, load(RESULT_4))

    def test_forbidden(self):
        temp = load(TEMPLATE_1)
        temp[merge.STRATEGY_KEY] = {'e': merge.FORBIDDEN_KEY}
        r = merge.merge(eh, {}, temp, load(SPEC_1))
        self.assertEqual(r, load(RESULT_5))
