#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import json
import subprocess
import sys
from typing import cast, Any, Dict, List, Tuple, Optional  # noqa: H301

import argparse
from kubernetes import client  # type: ignore
from kubernetes import config  # type: ignore
import logging
import os
import yaml

from . import baremetalpool
from . import byoh_cluster as byoh_cluster_manifest
from . import byoh_deployment
from . import byoh_kubeadm
from . import byoh_machines
from . import cluster as cluster_manifest
from . import config_util
from . import data
from . import deployment
from . import ipam
from . import kamaji_kubeadm
from . import kubeadm
from . import machine_healthcheck
from . import machines
from . import networkpool
from . import resourceset
from . import rke2
from . import updater
from . import versions

log = logging.getLogger(__name__)

ARGOCD_ENV_PREFIX = "ARGOCD_ENV_"


def read_yaml(name: str) -> config_util.YamlDict:
    with open(name, 'r') as fd:
        config = yaml.load(fd, Loader=yaml.Loader)
    return config


def read_yaml_all(name: str) -> List[config_util.YamlDict]:
    with open(name, 'r') as fd:
        config = list(yaml.load_all(fd, Loader=yaml.Loader))
    return config


def read_yaml_string(spec: str) -> config_util.YamlDict:
    return yaml.load(spec, Loader=yaml.Loader)


def generate_output(
    cluster: updater.Cluster,
    broker_data: Dict[str, Dict],
    output: str,
    additional: List[config_util.YamlDict]
):
    log.info(f'cluster generation {cluster.cluster_name}')
    cluster_spec = cluster_manifest.cluster_manifest(cluster)
    resource_list = [cluster_spec]

    if ("metal3" in cluster.infraProviders or
       "kamaji" in cluster.infraProviders):
        log.info('Metal3 cluster generation')
        metal3_cluster = cluster_manifest.metal3_cluster_manifest(cluster)
        resource_list.append(metal3_cluster)
    if "byoh" in cluster.infraProviders:
        log.info('BYOH cluster generation')
        byoh_cluster = byoh_cluster_manifest.byoh_cluster_manifest(cluster)
        resource_list.append(byoh_cluster)

    cp_spec = cast(Optional[config_util.YamlDict],
                   cluster.controlplane)
    if cp_spec is None:
        raise Exception('cluster must define a control-plane')
    log.info('Control plane')
    cp_version = versions.controlplane_version(cluster)
    if cp_version is None:
        cluster.error_handler.error(
            'validation',
            'Cannot proceed without Kubernetes version for'
            f'control-plane of {cluster.cluster_name}')
        return

    log.info('IpPools')
    ipam_networks, manifests = (
        ipam.ippools_manifests(cluster))
    resource_list += manifests

    log.info('Metadata')
    metadata_name, metadata = data.metadata_manifest(
        cluster, cluster.ippools_bindings)

    if "metal3" in cluster.infraProviders:
        resource_list += [metadata]

    log.info('Controlplane')
    if cluster.controlplane.get("infraProvider", "metal3") == "metal3":
        if cluster.rke2:
            controlplane = rke2.rke2_controlplane_manifest(cluster, cp_version)
        else:
            controlplane = kubeadm.kubeadm_controlplane_manifest(cluster,
                                                                 cp_version)
        cp_machine = machines.machine_template_manifest(
            cluster, None, cp_spec, metadata_name, cp_version)

        resource_list += [controlplane, cp_machine]

    if cluster.controlplane.get("infraProvider", "metal3") == "byoh":
        controlplane = byoh_kubeadm.byoh_kubeadm_controlplane_manifest(
            cluster, cp_version)
        cp_machine = byoh_machines.machine_template_manifest(
            cluster, None, cp_spec, metadata_name, cp_version)
        resource_list += [controlplane, cp_machine]

    if "byoh" in cluster.infraProviders:
        k8s_installer = byoh_machines.k8s_installer_manifest(
            cluster, None, cp_spec, metadata_name, cp_version)
        resource_list += [k8s_installer]

    if cluster.controlplane.get("infraProvider", "metal3") == "kamaji":
        controlplane = kamaji_kubeadm.kamaji_kubeadm_controlplane_manifest(
            cluster, cp_version)

        resource_list += [controlplane]

    log.info('Machine deployments')
    for md in cast(List[config_util.YamlDict], cluster.workers):
        if md.get("infraProvider", "metal3") == "metal3":
            resource_deployment = deployment.full_deployment(
                cluster, md, metadata_name)
            resource_list += resource_deployment

        if md.get("infraProvider", "metal3") == "byoh":
            manifests = byoh_deployment.full_deployment(
                cluster, md, metadata_name)

            resource_list += manifests

    log.info('Machine healthcheck')
    mhc_manifest = machine_healthcheck.worker_manifest(cluster)
    resource_list += [mhc_manifest]

    mhc_manifest = machine_healthcheck.cp_manifest(cluster)
    resource_list += [mhc_manifest]

    if cluster.need_autoscaler:
        resource_list += deployment.autoscaler(cluster)
    resource_list += resourceset.generate_inlined_addons(cluster, cp_version)

    log.info('Baremetalpool resources')
    pool_network_map = baremetalpool.get_pool_network_map(cluster)
    for bmpool in cast(List[config_util.YamlDict], cluster.baremetalpools):
        bm_resource = baremetalpool.baremetalpool_manifest(
            cluster,
            bmpool,
            broker_data["poolUserList"],
            pool_network_map,
            cp_version)
        resource_list += bm_resource

    log.info('ByoHostPoolScaler resources')
    if cluster.need_byohostpoolscaler:
        resource_list += baremetalpool.byohostpool_scaler(cluster)

    log.info('Network resources')
    for netpool in cast(List[config_util.YamlDict], cluster.networkpools):
        network_resource = networkpool.networkpool_manifest(
            cluster,
            netpool,
            broker_data["networkList"],
            ipam_networks)

        resource_list += network_resource
    resource_list += additional
    log.info('Full dump')
    with config_util.extended_open(output) as fd:
        yaml.dump_all(resource_list, stream=fd, explicit_start=True)


def getPoolUserAndNetworkList(cluster_name: str, cdef_namespace: str) -> Dict:
    error_handler = config_util.ErrorHandler()
    config.load_incluster_config()
    api = client.CustomObjectsApi()
    cdef = api.get_namespaced_custom_object(
        group='gitops.kanod.io',
        version="v1alpha1",
        namespace=cdef_namespace,
        plural="clusterdefs",
        name=cluster_name)

    data = {}
    if cdef is None:
        error_handler.error(
            'infra',
            f'Cannot read clusterdef {cluster_name} '
            'in namespace {cdef_namespace}')
        raise config_util.UpdaterException()

    if "poolUserList" in cdef["spec"]:
        data["poolUserList"] = cdef["spec"]["poolUserList"]

    if "networkList" in cdef["spec"]:
        data["networkList"] = cdef["spec"]["networkList"]

    return data


def update(
    conf_path: str,
    cluster_path: str,
    cluster_name: str,
    brokerdata_path: str,
    cidata_path: str,
    namespace: Optional[str],
    output: Optional[str]
):
    error_handler = config_util.ErrorHandler()
    try:
        config_util.setup_yaml_formatter()
        config = read_yaml(conf_path)
        descr = read_yaml(cluster_path)
        cidata = {}
        if cidata_path != "":
            cidata = cast(Dict[str, str], read_yaml(cidata_path))
        if brokerdata_path != "":
            brokerdata = cast(Dict[str, Dict[Any, Any]],
                              read_yaml(brokerdata_path))
        cluster = updater.Cluster(
            error_handler,
            cluster_name, config, descr, cidata, namespace=namespace)
        cluster.error_handler.abort_on_errors()
        if output is not None:
            generate_output(cluster, brokerdata, output, [])
        cluster.error_handler.abort_on_errors()
    except config_util.UpdaterException:
        for kind, msg in error_handler.errors:
            log.error(f'- {kind}: {msg}')
        exit(1)


def kubernetes_errors(
    name: str, namespace: str,
    error_handler: config_util.ErrorHandler,
    cluster: Optional[updater.Cluster]
):
    '''Report errors or the lack of through annotations of ClusterDef

    :param namespace: the namespace of the clusterdef resource
    :param name: the name of the clusterdef resource
    :param error_handler: the handler containing the errors.
    '''
    if cluster is not None:
        cpbound = cluster.limit_controlplane
        wkbound = cluster.limit_workers
    else:
        cpbound = None
        wkbound = None
    api = client.CustomObjectsApi()

    def dump(list: List[Tuple[str, str]]) -> Optional[str]:
        return None if list == [] else json.dumps(list)
    api.patch_namespaced_custom_object(
        group='gitops.kanod.io',
        version="v1alpha1",
        namespace=namespace,
        plural="clusterdefs",
        name=name,
        body={
            "metadata": {
                "annotations": {
                    "kanod.io/errors": dump(error_handler.errors),
                    "kanod.io/warnings": dump(error_handler.warnings),
                    "kanod.io/bound-controlplane": (
                        None if cpbound is None else str(cpbound)),
                    "kanod.io/bound-workers": (
                        None if wkbound is None else str(wkbound))
                }}
        })


def update_in_cluster(
    cdef_ns: str,
    conf_ns: str,
    conf_name: str,
    cidata_name: str,
    cluster_path: str,
    cluster_name: str,
    namespace: Optional[str],
    additional_path: Optional[str],
):
    error_handler = config_util.ErrorHandler()
    cluster_ref = None
    try:
        config_util.setup_yaml_formatter()
        descr = read_yaml(cluster_path)

        config.load_incluster_config()
        corev1 = client.CoreV1Api()
        cm = corev1.read_namespaced_config_map(conf_name, conf_ns)
        if cm is None:
            error_handler.error(
                'infra',
                f'Cannot find infrastructure configmap for {cluster_name}')
            raise config_util.UpdaterException()
        infra_text = cm.data.get('config', None)
        if infra_text is None:
            error_handler.error(
                'infra', 'config is not defined in infra configmap'
                f'for cluster {cluster_name}')
            raise config_util.UpdaterException()

        infra = read_yaml_string(infra_text)
        if cidata_name == "":
            log.info(f'No specific cidata found for {cluster_name}')
            cidata = {}
        else:
            cm_cidata = corev1.read_namespaced_config_map(cidata_name, conf_ns)
            cidata = cm_cidata.data
        cluster = updater.Cluster(
            error_handler,
            cluster_name, infra, descr, cidata, namespace=namespace)
        cluster_ref = cluster
        versions.get_current_k8s_version(cdef_ns, cluster)
        cluster.error_handler.abort_on_errors()
        broker_data = getPoolUserAndNetworkList(cluster_name, cdef_ns)
        additional = (
            read_yaml_all(additional_path)
            if additional_path is not None and os.path.exists(additional_path)
            else [])
        generate_output(cluster, broker_data, '-', additional)
        cluster.error_handler.abort_on_errors()
        kubernetes_errors(cluster_name, cdef_ns, error_handler, cluster)
    except config_util.UpdaterException:
        for kind, msg in error_handler.errors:
            log.error(f'- {kind}: {msg}')
        kubernetes_errors(cluster_name, cdef_ns, error_handler, cluster_ref)
        exit(1)


def vault_kustomize():
    os.environ['SSL_CERT_DIR'] = '/etc/ssl/certs:/app/config/tls'
    print(os.environ, file=sys.stderr)
    proc1 = subprocess.Popen(
        ['kustomize', 'build', '.'],
        stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding='utf-8')
    proc2 = subprocess.Popen(
        ['argocd-vault-plugin', 'generate', '-'],
        stdin=proc1.stdout, stderr=subprocess.PIPE, encoding='utf-8')
    proc1.stdout.close()
    ret1 = proc1.wait()
    if ret1 != 0:
        errs = proc1.communicate()[1]
        print(
            'kustomize failed: %d %s' % (ret1, errs),
            file=sys.stderr)
    ret2 = proc2.wait()
    if ret2 != 0:
        errs = proc2.communicate()[1]
        print(
            'vault plugin failed: %d %s' % (ret2, errs),
            file=sys.stderr)
        exit(1)
    print('Successful kustomize / vault plugin run', file=sys.stderr)
    exit(0)


def argocd_get(key, default):
    return os.environ.get(
        ARGOCD_ENV_PREFIX + key, os.environ.get(key, default))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-c", "--config",
        help="Configuration file for infra requirements"
    )
    parser.add_argument(
        "-d", "--discover", action=argparse.BooleanOptionalAction,
        help="Discover mode for argocd plugin"
    )
    parser.add_argument(
        "-k", "--kubernetes", action=argparse.BooleanOptionalAction,
        help="Parse for kubernetes mode from environment variables"
    )
    parser.add_argument(
        "-C", "--configmap",
        help="Configuration configmap name for infra requirements"
    )
    parser.add_argument(
        "-o", "--output",
        help="Output file for cluster-api (YAML)"
    )
    parser.add_argument(
        '-e', '--cdefns',
        help='Specify clusterdef namespace'
    )
    parser.add_argument(
        '-n', '--namespace',
        help='Specify output namespace'
    )
    parser.add_argument(
        "-N", "--cmns",
        help="Configuration configmap namespace for infra requirements"
    )
    parser.add_argument(
        "-m", "--cidata",
        help="Cloud-init metadata path to file or configmap name"
    )
    parser.add_argument(
        "-b", "--brokerdata",
        help="brokerdata path to file or configmap name"
    )
    parser.add_argument(
        "-l", "--clustername", help="cluster name"
    )
    parser.add_argument(
        "-p", "--cluster", help="cluster description"
    )
    args = parser.parse_args()
    if args.discover:
        if argocd_get('PLUGIN_MODE', None) is not None:
            print("OK")
        exit(0)
    elif args.kubernetes:
        mode = argocd_get('PLUGIN_MODE', '')
        if mode == 'vault':
            cmd = subprocess.run(
                ['argocd-vault-plugin', 'generate', './'],
                stderr=subprocess.PIPE, encoding='utf-8')
            if cmd.returncode != 0:
                print(
                    ('vault plugin failed: %d %s' %
                     (cmd.returncode, cmd.stderr)),
                    file=sys.stderr)
                exit(1)
        elif mode == 'vault-kustomize':
            vault_kustomize()
        elif mode == 'updater':
            update_in_cluster(
                cdef_ns=argocd_get('CDEF_NAMESPACE', ''),
                conf_ns=argocd_get('INFRA_NAMESPACE', ''),
                conf_name=argocd_get('INFRA_CONFIGMAP', ''),
                cidata_name=argocd_get('CIDATA_CONFIGMAP', ''),
                cluster_path=argocd_get('CLUSTER_CONFIG_FILE', ''),
                cluster_name=argocd_get('CLUSTER_NAME', ''),
                namespace=argocd_get('TARGET_NAMESPACE', ''),
                additional_path=argocd_get(
                    'ADDITIONAL_RESOURCE', 'additional.yaml')
            )
        else:
            if os.path.exists('kustomization.yaml'):
                if os.system('kustomize build --enable-helm .') != 0:
                    exit(1)
            else:
                os.system(
                    "find . \\( -name '*.yaml' -o -name '*.yml' \\) "
                    "-exec cat {} \\; -exec echo '---' \\;")
    elif args.configmap is not None:
        update_in_cluster(
            cdef_ns=args.cdefns, conf_ns=args.cmns, conf_name=args.configmap,
            cidata_name=args.cidata, cluster_path=args.cluster,
            cluster_name=args.clustername, namespace=args.namespace)
    else:
        update(
            conf_path=args.config, cluster_path=args.cluster,
            brokerdata_path=args.brokerdata, cidata_path=args.cidata,
            cluster_name=args.clustername,
            namespace=args.namespace, output=args.output)
