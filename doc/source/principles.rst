Principles of Cluster Synthesis
===============================

Problem Statement
-----------------
There are at least two roles who have stakes in the
definition of a cluster:

* The infrastructure manager manages the hardware
  and more specifically the network. He knows:

  * The network prefixes and available tunnels (VLANs, etc.)
  * The various address of the services required by the cluster (DNS, gateways, etc)

  He also knows the intrisic capacity of the available hardware.

* The cluster administrator knows the need of the cluster
  workloads and can select the resources to fulfill them.

The definition of the cluster parameters and more precisely
the node parameters should mix the requirements of those
actors.

Solution Overview
-----------------

Because those definitions are fully declarative in
Kanod, we can define responsibilities over the parts of the
configuration and build an evolutive **strategy** to
merge the two point of views. This strategy is ultimately
under the responsibility of the infrastructure manager.

The infrastructure manager define a set of templates
representing control-plane and worker node configurations. He
also define a set of addons templates for typically the network configuration. The templates are called **flavours** in Kanod. The clustor administrator select a flavour (implicitely the ``default`` flavour is selected if none is specified and the infrastructure administrator should at least define this flavour.) for each
kind of node used and for the addons. He then fills the remaining parameters.

There is an important different between node flavours and
addons flavours:

* node flavour are managed during the creation of nodes on
  the Kanod life cycle manager. They are part of the Metal3
  cluster definition.
* addons are deployed on the target cluster as soon as it is
  created.

All the data structure involved are defined in YAML and are
backed by JSON schemas.
