# kanod-updater
Builds a metal3 representation of a cluster from a configuration provided by a
cluster administrator and templates provided by an infrastructure manager.

The documentation is in folder doc