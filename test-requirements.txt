hacking>=4.0 # Apache-2.0
stestr>=1.0.0 # Apache-2.0
testtools>=1.4.0 # MIT
mypy>=0.930
jsonschema>=3.2.0
requests  >= 2.25.1
